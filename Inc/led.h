#include "stm32f4xx_hal.h"

typedef enum{
  LED_ON,
  LED_OFF
}ledState_t;

typedef enum{
  NO_TRANS=0,
  TRANS_OFF_ON=0x01,
  TRANS_ON_OFF=0x02,
  TRANS_X=0x04
  
}ledTransition_t;

typedef struct{
  ledState_t state;
  ledTransition_t trans;
  GPIO_TypeDef*  port;
  uint16_t pin;
  __IO uint32_t timeout;
  uint32_t period, duty;
}ledStruct;

void LED_LowLevelInit(GPIO_TypeDef*  port,uint16_t pin);
void LED_init(ledStruct * led, GPIO_TypeDef*  port,uint16_t pin, uint32_t period, uint32_t duty);

void LED_routines(ledStruct * led);
void LED_decrementDelay(ledStruct * led);



